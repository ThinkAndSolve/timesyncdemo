﻿namespace timeSyncDecmo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            timeSyncDecmo.MdlController mdlController1 = new timeSyncDecmo.MdlController();
            timeSyncDecmo.MdlController mdlController2 = new timeSyncDecmo.MdlController();
            timeSyncDecmo.MdlController mdlController3 = new timeSyncDecmo.MdlController();
            timeSyncDecmo.MdlController mdlController4 = new timeSyncDecmo.MdlController();
            timeSyncDecmo.MdlController mdlController5 = new timeSyncDecmo.MdlController();
            timeSyncDecmo.MdlController mdlController6 = new timeSyncDecmo.MdlController();
            timeSyncDecmo.MdlController mdlController7 = new timeSyncDecmo.MdlController();
            timeSyncDecmo.MdlController mdlController8 = new timeSyncDecmo.MdlController();
            timeSyncDecmo.MdlController mdlController9 = new timeSyncDecmo.MdlController();
            timeSyncDecmo.MdlController mdlController10 = new timeSyncDecmo.MdlController();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Event = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.ucMdlController10 = new timeSyncDecmo.ucMdlController();
            this.ucMdlController9 = new timeSyncDecmo.ucMdlController();
            this.ucMdlController8 = new timeSyncDecmo.ucMdlController();
            this.ucMdlController7 = new timeSyncDecmo.ucMdlController();
            this.ucMdlController6 = new timeSyncDecmo.ucMdlController();
            this.ucMdlController5 = new timeSyncDecmo.ucMdlController();
            this.ucMdlController4 = new timeSyncDecmo.ucMdlController();
            this.ucMdlController3 = new timeSyncDecmo.ucMdlController();
            this.ucMdlController2 = new timeSyncDecmo.ucMdlController();
            this.ucMdlController1 = new timeSyncDecmo.ucMdlController();
            this.label1 = new System.Windows.Forms.Label();
            this.lblTimeDeviation = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Event});
            this.dataGridView1.Location = new System.Drawing.Point(3, 420);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(1843, 301);
            this.dataGridView1.TabIndex = 8;
            // 
            // Event
            // 
            this.Event.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Event.HeaderText = "Event";
            this.Event.MinimumWidth = 6;
            this.Event.Name = "Event";
            this.Event.Width = 73;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // ucMdlController10
            // 
            this.ucMdlController10.Location = new System.Drawing.Point(1333, 217);
            this.ucMdlController10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            mdlController1.ControllerNumber = 0;
            mdlController1.LowestReceivesPrio = 0;
            mdlController1.PowerDown = false;
            mdlController1.PrioLevel = 14;
            mdlController1.SyncCount = 0;
            mdlController1.Time = new System.DateTime(2020, 1, 13, 12, 8, 59, 0);
            mdlController1.TimeSinceLastPrioTimeout = 247;
            this.ucMdlController10.Mdl = mdlController1;
            this.ucMdlController10.Name = "ucMdlController10";
            this.ucMdlController10.Size = new System.Drawing.Size(324, 198);
            this.ucMdlController10.TabIndex = 10;
            // 
            // ucMdlController9
            // 
            this.ucMdlController9.Location = new System.Drawing.Point(1333, 14);
            this.ucMdlController9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            mdlController2.ControllerNumber = 0;
            mdlController2.LowestReceivesPrio = 0;
            mdlController2.PowerDown = false;
            mdlController2.PrioLevel = 14;
            mdlController2.SyncCount = 0;
            mdlController2.Time = new System.DateTime(2020, 1, 13, 12, 8, 59, 0);
            mdlController2.TimeSinceLastPrioTimeout = 247;
            this.ucMdlController9.Mdl = mdlController2;
            this.ucMdlController9.Name = "ucMdlController9";
            this.ucMdlController9.Size = new System.Drawing.Size(324, 198);
            this.ucMdlController9.TabIndex = 9;
            // 
            // ucMdlController8
            // 
            this.ucMdlController8.Location = new System.Drawing.Point(1003, 217);
            this.ucMdlController8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            mdlController3.ControllerNumber = 0;
            mdlController3.LowestReceivesPrio = 0;
            mdlController3.PowerDown = false;
            mdlController3.PrioLevel = 14;
            mdlController3.SyncCount = 0;
            mdlController3.Time = new System.DateTime(2020, 1, 13, 12, 8, 59, 0);
            mdlController3.TimeSinceLastPrioTimeout = 247;
            this.ucMdlController8.Mdl = mdlController3;
            this.ucMdlController8.Name = "ucMdlController8";
            this.ucMdlController8.Size = new System.Drawing.Size(324, 198);
            this.ucMdlController8.TabIndex = 7;
            // 
            // ucMdlController7
            // 
            this.ucMdlController7.Location = new System.Drawing.Point(673, 217);
            this.ucMdlController7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            mdlController4.ControllerNumber = 0;
            mdlController4.LowestReceivesPrio = 0;
            mdlController4.PowerDown = false;
            mdlController4.PrioLevel = 14;
            mdlController4.SyncCount = 0;
            mdlController4.Time = new System.DateTime(2020, 1, 13, 12, 8, 59, 0);
            mdlController4.TimeSinceLastPrioTimeout = 247;
            this.ucMdlController7.Mdl = mdlController4;
            this.ucMdlController7.Name = "ucMdlController7";
            this.ucMdlController7.Size = new System.Drawing.Size(324, 198);
            this.ucMdlController7.TabIndex = 6;
            // 
            // ucMdlController6
            // 
            this.ucMdlController6.Location = new System.Drawing.Point(344, 217);
            this.ucMdlController6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            mdlController5.ControllerNumber = 0;
            mdlController5.LowestReceivesPrio = 0;
            mdlController5.PowerDown = false;
            mdlController5.PrioLevel = 14;
            mdlController5.SyncCount = 0;
            mdlController5.Time = new System.DateTime(2020, 1, 13, 12, 8, 59, 0);
            mdlController5.TimeSinceLastPrioTimeout = 247;
            this.ucMdlController6.Mdl = mdlController5;
            this.ucMdlController6.Name = "ucMdlController6";
            this.ucMdlController6.Size = new System.Drawing.Size(324, 198);
            this.ucMdlController6.TabIndex = 5;
            // 
            // ucMdlController5
            // 
            this.ucMdlController5.Location = new System.Drawing.Point(15, 217);
            this.ucMdlController5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            mdlController6.ControllerNumber = 0;
            mdlController6.LowestReceivesPrio = 0;
            mdlController6.PowerDown = false;
            mdlController6.PrioLevel = 14;
            mdlController6.SyncCount = 0;
            mdlController6.Time = new System.DateTime(2020, 1, 13, 12, 8, 59, 0);
            mdlController6.TimeSinceLastPrioTimeout = 247;
            this.ucMdlController5.Mdl = mdlController6;
            this.ucMdlController5.Name = "ucMdlController5";
            this.ucMdlController5.Size = new System.Drawing.Size(324, 198);
            this.ucMdlController5.TabIndex = 4;
            // 
            // ucMdlController4
            // 
            this.ucMdlController4.Location = new System.Drawing.Point(1003, 14);
            this.ucMdlController4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            mdlController7.ControllerNumber = 0;
            mdlController7.LowestReceivesPrio = 0;
            mdlController7.PowerDown = false;
            mdlController7.PrioLevel = 14;
            mdlController7.SyncCount = 0;
            mdlController7.Time = new System.DateTime(2020, 1, 13, 12, 8, 59, 0);
            mdlController7.TimeSinceLastPrioTimeout = 247;
            this.ucMdlController4.Mdl = mdlController7;
            this.ucMdlController4.Name = "ucMdlController4";
            this.ucMdlController4.Size = new System.Drawing.Size(324, 198);
            this.ucMdlController4.TabIndex = 3;
            // 
            // ucMdlController3
            // 
            this.ucMdlController3.Location = new System.Drawing.Point(673, 14);
            this.ucMdlController3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            mdlController8.ControllerNumber = 0;
            mdlController8.LowestReceivesPrio = 0;
            mdlController8.PowerDown = false;
            mdlController8.PrioLevel = 14;
            mdlController8.SyncCount = 0;
            mdlController8.Time = new System.DateTime(2020, 1, 13, 12, 8, 59, 0);
            mdlController8.TimeSinceLastPrioTimeout = 247;
            this.ucMdlController3.Mdl = mdlController8;
            this.ucMdlController3.Name = "ucMdlController3";
            this.ucMdlController3.Size = new System.Drawing.Size(324, 198);
            this.ucMdlController3.TabIndex = 2;
            // 
            // ucMdlController2
            // 
            this.ucMdlController2.Location = new System.Drawing.Point(344, 14);
            this.ucMdlController2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            mdlController9.ControllerNumber = 0;
            mdlController9.LowestReceivesPrio = 0;
            mdlController9.PowerDown = false;
            mdlController9.PrioLevel = 14;
            mdlController9.SyncCount = 0;
            mdlController9.Time = new System.DateTime(2020, 1, 13, 12, 8, 59, 0);
            mdlController9.TimeSinceLastPrioTimeout = 247;
            this.ucMdlController2.Mdl = mdlController9;
            this.ucMdlController2.Name = "ucMdlController2";
            this.ucMdlController2.Size = new System.Drawing.Size(324, 198);
            this.ucMdlController2.TabIndex = 1;
            // 
            // ucMdlController1
            // 
            this.ucMdlController1.Location = new System.Drawing.Point(15, 14);
            this.ucMdlController1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            mdlController10.ControllerNumber = 0;
            mdlController10.LowestReceivesPrio = 0;
            mdlController10.PowerDown = false;
            mdlController10.PrioLevel = 14;
            mdlController10.SyncCount = 0;
            mdlController10.Time = new System.DateTime(2020, 1, 13, 12, 8, 59, 0);
            mdlController10.TimeSinceLastPrioTimeout = 247;
            this.ucMdlController1.Mdl = mdlController10;
            this.ucMdlController1.Name = "ucMdlController1";
            this.ucMdlController1.Size = new System.Drawing.Size(324, 198);
            this.ucMdlController1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1664, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "Max time deviation:";
            // 
            // lblTimeDeviation
            // 
            this.lblTimeDeviation.AutoSize = true;
            this.lblTimeDeviation.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimeDeviation.Location = new System.Drawing.Point(1737, 79);
            this.lblTimeDeviation.Name = "lblTimeDeviation";
            this.lblTimeDeviation.Size = new System.Drawing.Size(31, 32);
            this.lblTimeDeviation.TabIndex = 12;
            this.lblTimeDeviation.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1860, 723);
            this.Controls.Add(this.lblTimeDeviation);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ucMdlController10);
            this.Controls.Add(this.ucMdlController9);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.ucMdlController8);
            this.Controls.Add(this.ucMdlController7);
            this.Controls.Add(this.ucMdlController6);
            this.Controls.Add(this.ucMdlController5);
            this.Controls.Add(this.ucMdlController4);
            this.Controls.Add(this.ucMdlController3);
            this.Controls.Add(this.ucMdlController2);
            this.Controls.Add(this.ucMdlController1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ucMdlController ucMdlController1;
        private ucMdlController ucMdlController2;
        private ucMdlController ucMdlController3;
        private ucMdlController ucMdlController4;
        private ucMdlController ucMdlController5;
        private ucMdlController ucMdlController6;
        private ucMdlController ucMdlController7;
        private ucMdlController ucMdlController8;
        private System.Windows.Forms.DataGridView dataGridView1;
        private ucMdlController ucMdlController9;
        private ucMdlController ucMdlController10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Event;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTimeDeviation;
    }
}

