﻿namespace timeSyncDecmo
{
    partial class ucMdlController
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbPowerDown = new System.Windows.Forms.CheckBox();
            this.lblSyncCount = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnNewTime = new System.Windows.Forms.Button();
            this.lblTime = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblPrioLevel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblControllerNumber = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblLowestReceivedPrio = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblPrioTimeout = new System.Windows.Forms.Label();
            this.lblBcInterval = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.panel1.Controls.Add(this.lblBcInterval);
            this.panel1.Controls.Add(this.lblPrioTimeout);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.lblLowestReceivedPrio);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.cbPowerDown);
            this.panel1.Controls.Add(this.lblSyncCount);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.btnNewTime);
            this.panel1.Controls.Add(this.lblTime);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lblPrioLevel);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.lblControllerNumber);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(244, 160);
            this.panel1.TabIndex = 0;
            // 
            // cbPowerDown
            // 
            this.cbPowerDown.AutoSize = true;
            this.cbPowerDown.Location = new System.Drawing.Point(139, 110);
            this.cbPowerDown.Name = "cbPowerDown";
            this.cbPowerDown.Size = new System.Drawing.Size(84, 17);
            this.cbPowerDown.TabIndex = 11;
            this.cbPowerDown.Text = "power down";
            this.cbPowerDown.UseVisualStyleBackColor = true;
            this.cbPowerDown.CheckedChanged += new System.EventHandler(this.cbPowerDown_CheckedChanged);
            // 
            // lblSyncCount
            // 
            this.lblSyncCount.AutoSize = true;
            this.lblSyncCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSyncCount.Location = new System.Drawing.Point(169, 3);
            this.lblSyncCount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSyncCount.Name = "lblSyncCount";
            this.lblSyncCount.Size = new System.Drawing.Size(18, 20);
            this.lblSyncCount.TabIndex = 10;
            this.lblSyncCount.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(101, 8);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Sync Count:";
            // 
            // btnNewTime
            // 
            this.btnNewTime.Location = new System.Drawing.Point(11, 110);
            this.btnNewTime.Margin = new System.Windows.Forms.Padding(2);
            this.btnNewTime.Name = "btnNewTime";
            this.btnNewTime.Size = new System.Drawing.Size(63, 48);
            this.btnNewTime.TabIndex = 7;
            this.btnNewTime.Text = "Receives New Time";
            this.btnNewTime.UseVisualStyleBackColor = true;
            this.btnNewTime.Click += new System.EventHandler(this.btnNewTime_Click);
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.Location = new System.Drawing.Point(89, 47);
            this.lblTime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(71, 20);
            this.lblTime.TabIndex = 6;
            this.lblTime.Text = "12:00:00";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 52);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Actual Time:";
            // 
            // lblPrioLevel
            // 
            this.lblPrioLevel.AutoSize = true;
            this.lblPrioLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrioLevel.Location = new System.Drawing.Point(89, 23);
            this.lblPrioLevel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPrioLevel.Name = "lblPrioLevel";
            this.lblPrioLevel.Size = new System.Drawing.Size(36, 20);
            this.lblPrioLevel.TabIndex = 4;
            this.lblPrioLevel.Text = "250";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 28);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Time prio level:";
            // 
            // lblControllerNumber
            // 
            this.lblControllerNumber.AutoSize = true;
            this.lblControllerNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblControllerNumber.Location = new System.Drawing.Point(57, 3);
            this.lblControllerNumber.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblControllerNumber.Name = "lblControllerNumber";
            this.lblControllerNumber.Size = new System.Drawing.Size(18, 20);
            this.lblControllerNumber.TabIndex = 2;
            this.lblControllerNumber.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "MDL #:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 76);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "prioTimeout:";
            // 
            // lblLowestReceivedPrio
            // 
            this.lblLowestReceivedPrio.AutoSize = true;
            this.lblLowestReceivedPrio.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLowestReceivedPrio.Location = new System.Drawing.Point(205, 73);
            this.lblLowestReceivedPrio.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblLowestReceivedPrio.Name = "lblLowestReceivedPrio";
            this.lblLowestReceivedPrio.Size = new System.Drawing.Size(18, 20);
            this.lblLowestReceivedPrio.TabIndex = 13;
            this.lblLowestReceivedPrio.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 95);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "broadcast Interval:";
            // 
            // lblPrioTimeout
            // 
            this.lblPrioTimeout.AutoSize = true;
            this.lblPrioTimeout.Location = new System.Drawing.Point(90, 76);
            this.lblPrioTimeout.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPrioTimeout.Name = "lblPrioTimeout";
            this.lblPrioTimeout.Size = new System.Drawing.Size(65, 13);
            this.lblPrioTimeout.TabIndex = 15;
            this.lblPrioTimeout.Text = "prioTimeout:";
            // 
            // lblBcInterval
            // 
            this.lblBcInterval.AutoSize = true;
            this.lblBcInterval.Location = new System.Drawing.Point(106, 95);
            this.lblBcInterval.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBcInterval.Name = "lblBcInterval";
            this.lblBcInterval.Size = new System.Drawing.Size(65, 13);
            this.lblBcInterval.TabIndex = 16;
            this.lblBcInterval.Text = "prioTimeout:";
            // 
            // ucMdlController
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "ucMdlController";
            this.Size = new System.Drawing.Size(243, 161);
            this.Load += new System.EventHandler(this.ucMdlController_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblSyncCount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnNewTime;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblPrioLevel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblControllerNumber;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbPowerDown;
        private System.Windows.Forms.Label lblLowestReceivedPrio;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblBcInterval;
        private System.Windows.Forms.Label lblPrioTimeout;
        private System.Windows.Forms.Label label5;
    }
}
