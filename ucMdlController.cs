﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace timeSyncDecmo
{
    public partial class ucMdlController : UserControl
    {
        public ucMdlController()
        {
            InitializeComponent();
        }
        public MdlController Mdl { get; set; }

        public event EventHandler<TimeBroadcastArgs> TimeMessageBroadcast;
        public event EventHandler<EventMessageArgs> EventMessageRaised;

        private void OnNewTime(TimeBroadcastArgs e)
        {
            if (TimeMessageBroadcast != null)
            {
                TimeMessageBroadcast(this, e);
            }
        }

        private void OnNewEventMsg(EventMessageArgs e)
        {
            if (EventMessageRaised != null)
            {
                EventMessageRaised(this, e);
            }
        }
        Random rd;
        int randomOffset;

        int broadCastIntervalTimer = 0;
        public int BroadCastInterval { private get; set; }
        Timer tmUpdate = new Timer();
        private void ucMdlController_Load(object sender, EventArgs e)
        {
            Mdl = new MdlController();
            tmUpdate.Tick += TmUpdate_Tick;
            tmUpdate.Interval = 1000;
            tmUpdate.Start();
            tmUpdate.Enabled = true;
            Mdl.Time = new DateTime(2020, 1, 13, 12, 0, 0);
            BroadCastInterval = 60;
            lblBcInterval.Text = BroadCastInterval.ToString();
        }

        public void SetColor(Color c)
        {
            panel1.BackColor = c;
        }

        private void TmUpdate_Tick(object sender, EventArgs e)
        {
            lblControllerNumber.Text = Mdl.ControllerNumber.ToString();
            int timeToAdd = 1000;

            if (Mdl.ControllerNumber % 2 == 0)
            {
                timeToAdd += (Mdl.ControllerNumber * 10);
            }
            else
            {
                timeToAdd -= (Mdl.ControllerNumber * 10);
            }
            Mdl.Time = Mdl.Time.AddMilliseconds(timeToAdd);
            lblTime.Text = Mdl.Time.ToLongTimeString();
            lblPrioLevel.Text = Mdl.PrioLevel.ToString();
            lblSyncCount.Text = Mdl.SyncCount.ToString();
            lblLowestReceivedPrio.Text = Mdl.LowestReceivesPrio.ToString();
            lblPrioTimeout.Text = Mdl.TimeSinceLastPrioTimeout.ToString() + " sec";

            if (Mdl.PowerDown == false)
            {
                if (Mdl.TimeSinceLastPrioTimeout > (120 + Mdl.RandomValue))
                {
                    Mdl.TimeSyncTimeOut = true;
                    if (Mdl.PrioLevel > 11)
                    {
                        Mdl.PrioLevel--;
                    }
                    //OnNewTime(new TimeBroadcastArgs(Mdl.Time, Mdl.PrioLevel, Mdl.ControllerNumber));
                    Mdl.TimeSinceLastPrioTimeout = 0;
                    Mdl.GenerateNewRandom();
                    OnNewEventMsg(new EventMessageArgs($"Controller {Mdl.ControllerNumber} hasn't seen lowest Prio for " +
                        $"{Mdl.TimeSinceLastPrioTimeout} sec and lowers own prio to: {Mdl.PrioLevel} time send is: {Mdl.Time.ToLongTimeString()}"
                        , eventType.EV_PRIOLOST_AND_LOWERED));
                    OnNewTime(new TimeBroadcastArgs(Mdl.Time, Mdl.PrioLevel, Mdl.ControllerNumber));
                }
                else
                {
                    Mdl.TimeSinceLastPrioTimeout++;
                }

                if (broadCastIntervalTimer < BroadCastInterval)
                {
                    broadCastIntervalTimer++;
                }
                else
                {

                    OnNewTime(new TimeBroadcastArgs(Mdl.Time, Mdl.PrioLevel, Mdl.ControllerNumber));
                    broadCastIntervalTimer = 0;
                    Random rd = new Random(Mdl.ControllerNumber * 1000);
                    BroadCastInterval = 60 + rd.Next(0, 15);
                    OnNewEventMsg(new EventMessageArgs($"Controller {Mdl.ControllerNumber} send time broadcast with Prio level: " +
                        $"{Mdl.PrioLevel} (time send is: {Mdl.Time.ToLongTimeString()})", eventType.EV_REGULAR_BROADCAST));

                }
                lblBcInterval.Text = $"{broadCastIntervalTimer}/{BroadCastInterval}";
            }
        }

        private void btnNewTime_Click(object sender, EventArgs e)
        {
            Mdl.Time = DateTime.Now;
            Mdl.PrioLevel = 10;
            OnNewTime(new TimeBroadcastArgs(Mdl.Time, Mdl.PrioLevel, Mdl.ControllerNumber));
            OnNewEventMsg(new EventMessageArgs($"Controller {Mdl.ControllerNumber} received new accurate time " +
                $"({Mdl.Time.ToLongTimeString()}) received from app and broadcasted", eventType.EV_NEW_TIME));
            Mdl.PrioLevel += 1;
        }

        private void cbPowerDown_CheckedChanged(object sender, EventArgs e)
        {
            Mdl.PowerDown = cbPowerDown.Checked;
            if (cbPowerDown.Checked)
            {
                btnNewTime.Enabled = false;
                SetColor(Color.LightGray);
                OnNewEventMsg(new EventMessageArgs($"Mdl controller {Mdl.ControllerNumber} is powered down", eventType.EV_POWERUP));
            } else
            {   Mdl.PrioLevel = 250;
                btnNewTime.Enabled = true;
                SetColor(Color.LightGreen);
                OnNewEventMsg(new EventMessageArgs($"Mdl controller {Mdl.ControllerNumber} is powered up", eventType.EV_POWERUP));
            }
        }

   
    }
    public class TimeBroadcastArgs : EventArgs
    {


        public TimeBroadcastArgs(DateTime time, int prio, int controllerNumber)
        {
            BroadcastedTime = time;
            PrioLevel = prio;
            ControllerNumber = controllerNumber;
        }

        // This is a straightforward implementation for 
        // declaring a public field
        public DateTime BroadcastedTime { get; private set; }
        public int PrioLevel { get; private set; }
        public int ControllerNumber { get; private set; }
    }
    public class EventMessageArgs : EventArgs
    {
        public EventMessageArgs(string msg, eventType type) {
            Message = msg;
            Type = type;
        }
        public string Message { get; private set; }
        public eventType Type { get; private set; }
    }
}
