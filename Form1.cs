﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace timeSyncDecmo
{
    public enum eventType { EV_NEW_TIME, EV_REGULAR_BROADCAST, EV_PRIOLOST_AND_LOWERED, EV_POWERUP, EV_TAKE_OVER };
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Random rnd = new Random(321675312);
            int mdlCounter = 1;
            foreach(Control c in Controls)
            {
                if (c is ucMdlController)
                {
                    ((ucMdlController)c).Mdl.ControllerNumber = mdlCounter++;
                    ((ucMdlController)c).TimeMessageBroadcast += Form1_TimeMessageBroadcast;
                    ((ucMdlController)c).EventMessageRaised += Form1_EventMessageRaised;
                    ((ucMdlController)c).BroadCastInterval = 60 + rnd.Next(0, 15);
                    ((ucMdlController)c).SetColor(Color.LightGreen);

                }
            }
            dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.AllowUserToDeleteRows = false;
            dataGridView1.AllowUserToOrderColumns = false;
            dataGridView1.ReadOnly = true;
            timer1.Enabled = true;
            timer1.Start();
        }

        private void Form1_EventMessageRaised(object sender, EventMessageArgs e)
        {
            AddEvent(e.Message, e.Type);
        }

        private void Form1_TimeMessageBroadcast(object sender, TimeBroadcastArgs e)
        {
            //MessageBox.Show($"MDL controller {e.ControllerNumber} has broadcasted a new time of {e.BroadcastedTime.ToLongTimeString()}, with Prio level {e.PrioLevel}");
            foreach (Control c in Controls)
            {
                int senderId = 0;
                if (sender is ucMdlController)
                {
                    senderId = ((ucMdlController)sender).Mdl.ControllerNumber;
                }
                if (c is ucMdlController)
                {
                    if (((ucMdlController)c).Mdl.ReceivesNewTime(e.BroadcastedTime, e.PrioLevel) == true)
                    {
                        AddEvent($"Controller {((ucMdlController)c).Mdl.ControllerNumber} has taken over time from controller " +
                            $"{senderId}", eventType.EV_TAKE_OVER);
                    }
                }
            }
        }

        
        private void AddEvent(string msg, eventType type)
        {
            int rowIndex = dataGridView1.Rows.Add();
            dataGridView1.Rows[rowIndex].HeaderCell.Value = DateTime.Now.ToLocalTime().ToLongTimeString();
            dataGridView1.Rows[rowIndex].Cells["Event"].Value = msg;
            switch (type)
            {
                case eventType.EV_NEW_TIME:
                    dataGridView1.Rows[rowIndex].DefaultCellStyle.BackColor = Color.LightBlue;
                    break;
                case eventType.EV_POWERUP:
                    dataGridView1.Rows[rowIndex].DefaultCellStyle.BackColor = Color.LightYellow;
                    break;
                case eventType.EV_PRIOLOST_AND_LOWERED:
                    dataGridView1.Rows[rowIndex].DefaultCellStyle.BackColor = Color.Red;
                    break;
                case eventType.EV_REGULAR_BROADCAST:
                    dataGridView1.Rows[rowIndex].DefaultCellStyle.BackColor = Color.LightGreen;
                    break;
                case eventType.EV_TAKE_OVER:
                    dataGridView1.Rows[rowIndex].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                    break;
                default:
                    break;

            }
        }

        private int GetTimeInSec(DateTime time)
        {
            int timeInSec = (time.Hour * 3600) + (time.Minute * 60) + time.Second;
            return timeInSec;
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            int lowestPrio = 255;
            int lowestTime = 86400;
            int highestTime = 0;
            bool setInitialPrio = true;
            bool allEqual = true;
            ucMdlController timeMaster;
            foreach (Control c in Controls)
            {
                if (c is ucMdlController) { 

                    //if (setInitialPrio)
                    //{
                    //    setInitialPrio = false;
                    //    lowestPrio = ((ucMdlController)c).Mdl.PrioLevel;
                    //}

                    if (!((ucMdlController)c).Mdl.PowerDown)
                    {
                        if (((ucMdlController)c).Mdl.PrioLevel < lowestPrio)
                        {
                            lowestPrio = ((ucMdlController)c).Mdl.PrioLevel;
                        }
                        if (((ucMdlController)c).Mdl.PrioLevel != lowestPrio)
                        {
                            allEqual = false;
                        }
                    }
                    
                }
            }
            int nrOfLowest = 0;
            foreach (Control c in Controls)
            {
                if (c is ucMdlController)
                {
                    if (!((ucMdlController)c).Mdl.PowerDown)
                    {
                        if (((ucMdlController)c).Mdl.PrioLevel == lowestPrio)
                        {
                            //this is the time master
                            nrOfLowest++;
                            if (!allEqual)
                            {
                                ((ucMdlController)c).SetColor(Color.LightBlue);
                            }
                        } else
                        {
                            ((ucMdlController)c).SetColor(Color.LightGreen);
                        }
                        if (GetTimeInSec(((ucMdlController)c).Mdl.Time) < lowestTime)
                        {
                            lowestTime = GetTimeInSec(((ucMdlController)c).Mdl.Time);
                        }
                        if (GetTimeInSec(((ucMdlController)c).Mdl.Time) > highestTime)
                        {
                            highestTime = GetTimeInSec(((ucMdlController)c).Mdl.Time);
                        }

                        int deviation = highestTime - lowestTime;
                        lblTimeDeviation.Text = $"{deviation} sec";

                    }
                    else
                    {
                        ((ucMdlController)c).SetColor(Color.LightGray);
                    }
                }
            }
            if (nrOfLowest > 1)
            {
                //there is no leading time master, paint all green again
                foreach (Control c in Controls)
                {
                    if (c is ucMdlController)
                    {
                        if (!((ucMdlController)c).Mdl.PowerDown)
                        {
                            ((ucMdlController)c).SetColor(Color.LightGreen);
                        }
                    }
                }
            }
        }
    }
}
