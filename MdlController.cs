﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace timeSyncDecmo
{
    public class MdlController
    {

        Random rd;
        private int controllerNr;
        private int pRandomInt;
        public int ControllerNumber
        {
            get
            {
                return controllerNr;
            }
            set
            {
                controllerNr = value;
                rd = new Random(controllerNr * 1000);
                if ((controllerNr == 2)
                    || (controllerNr == 8)
                    )
                {
                    pRandomInt = 0;
                }
                else
                {
                    pRandomInt = rd.Next(0, 20);
                }

            }
        }
        public void GenerateNewRandom()
        {
            if ((controllerNr == 2)
                    || (controllerNr == 8)
                    )
            {
                pRandomInt = 0;
            }
            else
            {
                pRandomInt = rd.Next(0, 20);
            }
        }
        public int RandomValue
        {
            get
            {
                return pRandomInt;
            }
        }
        public DateTime Time { get; set; }
        public bool PowerDown { get; set; }
        
        public int SyncCount { get; set; }
        public int PrioLevel { get; set; }

        public bool TimeSyncTimeOut { get; set; }

        public int LowestReceivesPrio { get; set; }
        //private int _LowestPrioReceivedTimeout;

        public int TimeSinceLastPrioTimeout
        {
            get; set;
        }
        //} {
        //        return _LowestPrioReceivedTimeout;
        //    }
        //}
        //public void IncreasePrioReceivedTimeout(int nrOfSec)
        //{
        //    _LowestPrioReceivedTimeout += nrOfSec;
        //    if (_LowestPrioReceivedTimeout > (300 + ControllerNumber))
        //    {
        //        PrioLevel--;
        //        _LowestPrioReceivedTimeout = 0;
        //    }
        //}
    
        public bool ReceivesNewTime(DateTime newTime, int prioLevel)
        {
            bool TimeTakenOver = false;
            if (PowerDown) {
                return TimeTakenOver;
            }
            if (prioLevel < PrioLevel) {
                TimeSinceLastPrioTimeout = 0;
                LowestReceivesPrio = prioLevel;
                Time = newTime;
                PrioLevel = prioLevel + 2;
                TimeTakenOver = true;
                SyncCount++;
            }
            return TimeTakenOver;

        }
        public MdlController() {
            PrioLevel = 15;
        }

    }
}
